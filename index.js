var request = require('request'),
    _ = require('underscore'),
    moment = require('moment');

var ShippingCompany = function() {
    // tulis kode di sini
    var ships = [],
        callback = arguments[arguments.length - 1];

    // buat companyName
    this.companyName = arguments[0];

    // fungsi registerShip
    this.registerShip = function(ship) {
        var _ship = {};
        for (var property in ship) {
            if (typeof ship[property] !== 'function' && _.has(ship, property)) {
                _ship[property] = ship[property];
            }
        }
        ships.push(_ship);
    };

    // fungsi list semua ships
    this.listAllShips = function() {
        return ships;
    };

    // ensure that this company name is set
    if (typeof this.companyName === 'string') {
        callback(true);
    } else {
        callback(false);
    }

};

var Ship = function() {
    // tulis kode di sini
    var _this = this;
    var arg = arguments;

    // create voyage
    var voyage = {};
    var shipProperty = {};
    var security = 0;

    var init = function() {

        if (typeof arg[0] !== 'object') {
            _this.shipName = arg[0];
            _this.callSign = arg[1];
            _this.length = arg[2];
            _this.beam = arg[3];
            _this.dwt = arg[4];

            var callback = arg[5];
            callback(true);
        } else {
            var properties = arg[0];
            _this.shipName = properties.shipName || null;
            _this.callSign = properties.callSign || null;
            _this.length = properties.length || null;
            _this.beam = properties.beam || null;
            _this.dwt = properties.dwt || null;
        }
    };

    var getVoyage = function() {
        return _this.voyage;
    };

    this.goVoyage = function(destination, date) {
        voyage.destination = destination;
        voyage.date = new Date(date);
    };

    this.whereToGo = function(callback) {
        callback(voyage.destination);
    };

    this.isAvailable = function(date, callback) {
        callback(moment(voyage.date).isBefore(date));
    };

    this.placeSecurity = function(count) {
        security = count;
    };

    this.attackedByPirates = function(numPirates, callback) {
        if (security >= numPirates) {
            callback(false);
        } else {
            callback(true);
        }
    };

    this.whenArrive = function(countingArrives, callback) {
        var b = moment(countingArrives);
        var a = moment(voyage.date);
        var diff = a.diff(b, 'days');
        callback(diff + ' days');
    };

    init();
    return _this;
};

function downloadShipCatalog(callback) {
    // resource url
    var resource = 'http://private-25337-qiscuschallenge.apiary-mock.com/ships';

    // request resource
    request(resource, {
        json: true
    }, function(error, response, body) {
        // if not error and response is OK
        if (!error && response.statusCode === 200) {
            callback(body);
        } else {
            callback(false);
        }
    });
}

(function() {
    /*
     * Popeye telah menyelesaikan semua perizinan dan administrasi
     * perusahaan.Itu artinya perusahaannya kini sudah resmi
     * berdiri.
     *
     */

    var alsCompany = new ShippingCompany('PT ASL', function(result) {
        console.log('1  : ' + result);
    });

    /*
     * Namun demikian, perusahaan tersebut masih belum bisa beroperasi
     * karena Popeye belum memiliki kapal. Kapten Haddock,
     * yang merupakan teman sesama pelaut, mendengar bahwa Popeye baru
     * mendirikan perusahaan dan tertarik untuk berinvestasi dengan
     * meminjamkan satu buah kapal miliknya. Popeye pun senang karena
     * perusahaannya bisa segera beroperasi.
     *
     * Kapal yang dipinjamkan Kapten Haddock memiliki nama MSC Sabrina
     * dengan call sign 3FMG8. MSC Sabrina memiliki panjang 240 meter,
     * lebar 32 meter, dan bobot mati 43000 ton.
     *
     */

    var mscSabrina = new Ship('MSC Sabrina', '3FMG8', '240', '32', '43000', function(result) {
        console.log('2  : ' + result);
    });

    /*
     * Karena kapal yang dimilikinya dirasa kurang, Popeye akhirnya
     * memutuskan untuk membeli kapal lagi. Selang beberapa waktu, seorang
     * salesman dari perusahaan penyedia kapal, mengirimi Popeye katalog
     * melalui surat elektronik.
     * Ia pun mengunduh katalog tersebut.
     *
     */

    // bantuan: gunakan ajax get untuk mendapatkan isi katalog tersebut
    // melalui url http://private-25337-qiscuschallenge.apiary-mock.com/ships

    downloadShipCatalog(function(shipCatalog) {
        /* Dari katalog tersebut, Popeye tertarik untuk membeli kapal
         * MV Virginians, MV Tampa, dan MSC Cordoba.
         *
         */

        var mvVirginians = new Ship(_(shipCatalog).findWhere({
            shipName: 'MV Virginians'
        }));

        var mvTampa = new Ship(_(shipCatalog).findWhere({
            shipName: 'MV Tampa'
        }));
        var mscCordoba = new Ship(_(shipCatalog).findWhere({
            shipName: 'MSC Cordoba'
        }));


        /*
         * Kini Popeye telah memiliki empat buah kapal. Ia melakukan
         * pendataan terhadap kapal yang dimiliki oleh perusahaannya.
         *
         */

        alsCompany.registerShip(mscSabrina);
        alsCompany.registerShip(mvVirginians);
        alsCompany.registerShip(mvTampa);
        alsCompany.registerShip(mscCordoba);

        var shipList = alsCompany.listAllShips();

        /*
         * Semua kapal telah didata, dan kini saatnya berlayar.
         * Setiap kapal memiliki tujuan masing-masing, dan semuanya telah
         * dijadwalkan untuk sampai kembali berdasarkan tanggal yang telah
         * ditentukan.
         *
         */

        mscSabrina.goVoyage('MADAGASCAR', '2015-02-01');
        mvVirginians.goVoyage('SOUTH KOREA', '2015-01-20');
        mvTampa.goVoyage('CANADA', '2015-02-10');
        mscCordoba.goVoyage('FRANCE', '2015-01-30');

        /*
         * Popeye sering lupa kemana tujuan masing-masing kapalnya. Maka dari
         * itu ia sering melihat catatan untuk memastikannya.
         */

        mscSabrina.whereToGo(function(result) {
            console.log('3  : ' + result);
        });
        mscCordoba.whereToGo(function(result) {
            console.log('4  : ' + result);
        });

        /*
         * Di antara keempat kapalnya, MSC Cordoba lah yang lebih sering
         * dipakai untuk mengantarkan peti kemas. Ini karena ukurannya yang
         * sangat besar sehingga bisa memuat peti kemas lebih banyak.
         *
         * Tak jarang Popeye harus memeriksa apakah MSC Cordoba bisa
         * mengantar peti kemas sesuai dengan tanggal pesanan. Apabila pada
         * tanggal tersebut MSC Cordoba telah kembali, berarti ia tersedia
         * untuk digunakan.
         *
         */

        mscCordoba.isAvailable('2015-01-20', function(result) {
            console.log('5  : ' + result);
        });

        mscCordoba.isAvailable('2015-02-02', function(result) {
            console.log('6  : ' + result);
        });

        /* Untuk menjaga keamanan, Popeye menempatkan petugas keamanan yang
         * jumlahnya berbeda untuk masing-masing kapal.
         *
         */

        mscSabrina.placeSecurity(3);
        mvVirginians.placeSecurity(2);
        mvTampa.placeSecurity(4);
        mscCordoba.placeSecurity(5);

        /* Dan benar saja, Si Brutus, preman yang suka mengganggu Popeye,
         * berniat untuk membajak kapal. Ia mengutus empat kelompok perompak
         * yang masing-masing beranggotakan dua orang untuk membajak keempat
         * kapal Popeye yang sedang berlayar.
         *
         * Keselamatan dan keamaan kapal Popeye bergantung pada petugas
         * keamanan yang sedang berjaga. Apabila jumlah petugas keamanan sama
         * atau lebih dari jumlah perompak, maka perompak tersebut gagal untuk
         * membajak kapal Popeye.
         *
         */

        mscSabrina.attackedByPirates(2, function(result) {
            console.log('7  : ' + result);
        });
        mvVirginians.attackedByPirates(2, function(result) {
            console.log('8  : ' + result);
        });
        mvTampa.attackedByPirates(2, function(result) {
            console.log('9  : ' + result);
        });
        mscCordoba.attackedByPirates(2, function(result) {
            console.log('10 : ' + result);
        });

        /*
         * Syukurlah semua kapal berhasil selamat dari serangan perompak.
         * Popeye merasa senang sekaligus khawatir. Ia menghitung berapa hari
         * lagi semua kapalnya akan kembali.
         *
         */

        mscSabrina.whenArrive(Date.now(), function(result) {
            console.log('11 : ' + result);
        });
        mvVirginians.whenArrive(Date.now(), function(result) {
            console.log('12 : ' + result);
        });
        mvTampa.whenArrive(Date.now(), function(result) {
            console.log('13 : ' + result);
        });
        mscCordoba.whenArrive(Date.now(), function(result) {
            console.log('14 : ' + result);
        });

    });



})();